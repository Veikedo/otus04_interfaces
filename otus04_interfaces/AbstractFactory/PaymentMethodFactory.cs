﻿using otus04_interfaces.StrategyPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.AbstractFactory
{
	class PaymentMethodFactory : IPaymentMethodFactory
	{
		public IPay CreatePaymentMethod()
		{
			IPay pay;
			Console.WriteLine("Нажмите 1, для оплаты VISA или 2 для оплаты Google Pay");
			var userAnswer = Console.ReadLine();
			switch (userAnswer)
			{
				case "1":
					pay = new VisaPay();
					break;
				case "2":
					pay = new GooglePay();
					break;
				default:
					throw new Exception("Не выбран способ оплаты");
			}
			return pay;
		}
	}
}
