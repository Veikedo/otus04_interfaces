﻿using System;
using System.Collections.Generic;
using System.Linq;
using otus04_interfaces.StrategyPattern;

namespace otus04_interfaces.AbstractFactory
{
	interface IPaymentMethodFactory
	{
		IPay CreatePaymentMethod();
	}
}
