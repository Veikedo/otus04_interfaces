﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace otus04_interfaces.HomeWork
{
	class AccountRepositoryCsv : IRepository<Account>
	{
		private string CsvFilePath { get; set; }
		public AccountRepositoryCsv(string path)
		{
			CsvFilePath = path;
			if (!File.Exists(path))
				File.WriteAllText(path, string.Empty);
		}
		public void Add(Account item)
		{
			if (!Exists(a => a.FirstName == item.FirstName && a.LastName == item.LastName && a.BirthDate == item.BirthDate))
			{
				var contents = new string[]
				{
					string.Join(";", item.FirstName, item.LastName, item.BirthDate)
				};
				File.AppendAllLines(CsvFilePath, contents);
			}
		}

		public IEnumerable<Account> GetAll()
		{
			var accountList = new List<Account>();
			var text = File.ReadAllText(CsvFilePath);
			var rows = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			foreach(var row in rows)
			{
				var columns = row.Split(new string[] { ";" }, StringSplitOptions.None);
				if (columns.Length < 3) continue;
				accountList.Add(new Account()
				{
					FirstName = columns[0],
					LastName = columns[1],
					BirthDate = DateTime.Parse(columns[2])
				});
			}
			return accountList;
		}


		public Account GetOne(Func<Account, bool> predicate)
		{
			return GetAll().FirstOrDefault(a => predicate(a));
		}

		public bool Exists(Func<Account, bool> predicate)
		{
			return GetOne(predicate) != null;
		}
	}
}
