﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace otus04_interfaces.HomeWork
{
	class Work1
	{
		public XmlEnumerable Start()
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(Properties.Resources.XMLFile1);
			var xmlEnumerable = new XmlEnumerable(xmlDoc);

			Console.WriteLine("Before sort");

			//До сортировки
			foreach (var element in xmlEnumerable) Console.WriteLine($"Элемент: {element.Name}");

			return xmlEnumerable;
		}
	}
}
