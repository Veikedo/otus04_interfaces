﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.HomeWork
{
	public class AccountGenerator : IEnumerable<Account>
	{
		public IEnumerator<Account> GetEnumerator()
		{
			yield return new Account()
			{
				FirstName = "Николай",
				LastName = "Петров",
				BirthDate = new DateTime(1990, 2, 16)
			};

			yield return new Account()
			{
				FirstName = "Кирилл",
				LastName = "Невский",
				BirthDate = new DateTime(1995, 9, 28)
			};

			yield return new Account()
			{
				FirstName = "Валерия",
				LastName = "Брежнева",
				BirthDate = new DateTime(1995, 8, 10)
			};

			yield return new Account()
			{
				FirstName = "Рыбина",
				LastName = "Рыболовная",
				BirthDate = new DateTime()
			};
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
