﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.HomeWork
{
	class Work2
	{
		public void Start(XmlEnumerable xmlEnumerable)
		{
			//Рандомная сортировка
			xmlEnumerable.SortRandom();

			Console.WriteLine();
			Console.WriteLine("After sort");

			//Полсе рандомной сортировки
			foreach (var element in xmlEnumerable) Console.WriteLine($"Элемент: {element.Name}");

			//Проверка LINQ
			var firstNode = xmlEnumerable.FirstOrDefault();
		}
	}
}
