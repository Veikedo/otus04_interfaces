﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.DecoratorPattern
{
	interface ISavable
	{
		void Save();
	}
}
