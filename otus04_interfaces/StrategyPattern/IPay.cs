﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.StrategyPattern
{
	interface IPay
	{
		void Pay(decimal payAmount, Action<bool> afterPayCallback);
	}
}
