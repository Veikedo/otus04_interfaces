﻿using otus04_interfaces.AbstractFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus04_interfaces.StrategyPattern
{
	class OnlineShop
	{
		ProductCart _cart;
		IPaymentMethodFactory _paymentMethodFactory;
		public OnlineShop(IPaymentMethodFactory paymentMethodFactory)
		{
			_paymentMethodFactory = paymentMethodFactory ?? throw new ArgumentNullException("Выбор способа оплаты (paymentMethodFactory)");
			_cart = new ProductCart();
		}
		public void AddProductToCart(string productName) => _cart.Add(new Product(productName));
		public void RemoveProductFromCart(string productName) => _cart.Remove(new Product(productName));
		public bool CanBuy => _cart.Count > 0;
		public decimal PaySum => _cart.GetCost();
		public void Buy()
		{
			if (!CanBuy)
			{
				Console.WriteLine("Не выбрано ни одного товара");
				return;
			}
			//Паттерн фабрика
			var paymentMethod = _paymentMethodFactory.CreatePaymentMethod();
			paymentMethod.Pay(PaySum, AfterUserPay);
		}
		private void AfterUserPay(bool success)
		{
			if (success)
			{
				_cart.RemoveAll(p => true);
				Console.WriteLine("Поздравляем с покупкой");
			}
			else
			{
				Console.WriteLine("Не удалось совершить покупку");
			}
		}
	}
}
