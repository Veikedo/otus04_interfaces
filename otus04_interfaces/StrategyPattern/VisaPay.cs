﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus04_interfaces.StrategyPattern
{
	class VisaPay : IPay
	{
		public void Pay(decimal payAmount, Action<bool> afterPayCallback)
		{
			Console.WriteLine("Введите данные карты");
			var cardInfo = Console.ReadLine();
			if (!string.IsNullOrEmpty(cardInfo))
			{
				Console.WriteLine($"C карты успешно списано {payAmount}");
				afterPayCallback?.Invoke(true);
			}
			else
			{
				Console.WriteLine($"C карты не удалось списать {payAmount}");
				afterPayCallback?.Invoke(false);
			}
		}
	}
}
