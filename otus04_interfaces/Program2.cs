﻿using System;
using otus04_interfaces.StrategyPattern;
using otus04_interfaces.AbstractFactory;
using otus04_interfaces.DecoratorPattern;
using otus04_interfaces.CovarianceContravariance;

namespace otus04_interfaces
{
	class Program2
	{
		static void Main2(string[] args)
		{
			//Стратегия и абстрактная фабрика
			//Act1();

			//Декоратор
			//Act2();

			//Ковариация
			//Act3();
			//Act5();

			//Контрвариация
			//Act4();
			//Act6();


			Console.ReadKey();
		}

		private static void Act1()
		{
			//Абстрактная фабрика
			var factory = new PaymentMethodFactory();

			//Паттерн стратегия - мы передали реализацию на вход, абстрагировавшись от реализации /Можно указывать в сист настройках
			var internShop = new OnlineShop(factory);

			internShop.AddProductToCart("Бусы");
			internShop.Buy();
		}

		private static void Act2()
		{
			//Что-то что должно быть сохранено
			var smthNeedBeSaved = new SaverX();

			//Декоратор
			var smthNeedBeSavedWithEvents = new SavableWithEvents(smthNeedBeSaved);
			smthNeedBeSavedWithEvents.OnSaving += (s, a) => Console.WriteLine("OnSaving");
			smthNeedBeSavedWithEvents.OnSaved += (s, a) => Console.WriteLine("OnSaved");

			smthNeedBeSavedWithEvents.Save();
		}

		private static void Act3()
		{
			IBank<DepositAccount> depositBank = new Bank<DepositAccount>();
			var acc1 = depositBank.CreateAccount(34);

			IBank<Account> ordinaryBank = new Bank<DepositAccount>();
			Account acc2 = ordinaryBank.CreateAccount(45);
		}

		private static void Act4()
		{
			ITransaction<Account> accTransaction = new Transaction<Account>();
			accTransaction.DoOperation(new Account(), 400);

			ITransaction<DepositAccount> depAccTransaction = new Transaction<Account>();
			depAccTransaction.DoOperation(new DepositAccount(), 450);
		}



		delegate T Builder<out T>(string name);
		static void Act5()
		{
			Builder<Client> clientBuilder = GetClient;
			Builder<Person> personBuilder1 = clientBuilder;     // ковариантность
			Builder<Person> personBuilder2 = GetClient;         // ковариантность

			Person p = personBuilder1("Tom"); // вызов делегата
			p.Display(); // Client: Tom
		}
		private static Person GetPerson(string name)
		{
			return new Person { Name = name };
		}
		private static Client GetClient(string name)
		{
			return new Client { Name = name };
		}


		delegate void GetInfo<in T>(T item);
		static void Act6()
		{
			GetInfo<Person> personInfo = PersonInfo;
			GetInfo<Client> clientInfo = personInfo;      // контравариантность

			Client client = new Client { Name = "Tom" };
			clientInfo(client); // Client: Tom
		}
		private static void PersonInfo(Person p) => p.Display();
		private static void ClientInfo(Client cl) => cl.Display();
	}



}
